<%-- 
    Document   : home
    Created on : Mar 14, 2015, 2:28:07 PM
    Author     : danh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<t:layout layoutTitle="Demo" layoutHeader="Demonstration">
    <jsp:body>  
        <form class="query7Form" action="" method="GET" accept-charset="utf-8">
            <ul>
                <li>
                    <label>Query</label>
                    <input class="queryTxt" type="text" name="os" required="true">
                </li>
                <div class="leftSide">
                    <h4>Crowd Hints</h4>
                    <li>
                        <label></label>
                        <input type="submit" class="query7Btn" value="Query">
                        
                    </li>
                    </li>
                    <div  class="customerTable" hidden="true">
                    </div>
                </div>

                <div class="rightSide">
                    <h4>BM25</h4>
                    <li>
                        <label></label>
                        <input type="submit" class="query7RightBtn" value="Query"> 
                    </li>
                    </li>
                    <div  class="customerTableRight" hidden="true">
                    </div>
                </div>
            </ul>
        </form>
    </jsp:body>
</t:layout>