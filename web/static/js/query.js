/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(".query7Btn").click(function (e) {
    var os = $(".queryTxt").val();

    if (os !== undefined) {
        $.get("./service?engine=crowdhints&query=" + os, function (data) {
            $(".customerTable").fadeOut("fast", function () {
                $(".customerTable").html(data);
                $(".customerTable").fadeIn("fast");
            });

        });
    }
    e.preventDefault();
});

$(".query7RightBtn").click(function (e) {
    var os = $(".queryTxt").val();

    if (os !== undefined) {
        $.get("./service?engine=bm25&query=" + os, function (data) {
            $(".customerTableRight").fadeOut("fast", function () {
                $(".customerTableRight").html(data);
                $(".customerTableRight").fadeIn("fast");
            });

        });
    }
    e.preventDefault();
});