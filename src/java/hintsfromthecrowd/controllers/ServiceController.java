/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hintsfromthecrowd.controllers;

import hintsfromthecrowd.engines.CrowdHintsEngine;
import hintsfromthecrowd.engines.CustomizedBM25Engine;
import hintsfromthecrowd.models.Product;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author danh
 */
@WebServlet(name = "ServiceController", urlPatterns = {"/service"})
public class ServiceController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String engine = request.getParameter("engine");
        String query = request.getParameter("query");
        RequestDispatcher rd;
        List<Product> result;
        double timeSecond;

        try {
            if (engine == null || query == null) {
                response.sendRedirect("./404");
                return;
            }

            switch (engine) {

                case "crowdhints":
                    CrowdHintsEngine crowdhints = new CrowdHintsEngine();
                    //get result
                    //start timer
                    long tStart = System.currentTimeMillis();
                    result = crowdhints.run(query);
                    long tEnd = System.currentTimeMillis();
                    long tDelta = tEnd - tStart;
                    timeSecond = tDelta;
                    //end timecounter
                    request.setAttribute("total", result.size());
                    result = crowdhints.filResult(result);
                    request.setAttribute("products", result);
                    request.setAttribute("time", timeSecond);
                    rd = request.getRequestDispatcher("productTable.jsp");
                    rd.forward(request, response);
                    break;
                case "bm25":
                    CustomizedBM25Engine bm25 = new CustomizedBM25Engine();
                    //get result
                    //start timer
                    tStart = System.currentTimeMillis();
                    result = bm25.run(query);
                    tEnd = System.currentTimeMillis();
                    tDelta = tEnd - tStart;
                    timeSecond = tDelta;
                    //end timecounter
                    request.setAttribute("total", result.size());
                    result = bm25.filResult(result);
                    request.setAttribute("products", result);
                    request.setAttribute("time", timeSecond);

                    rd = request.getRequestDispatcher("productTable.jsp");
                    rd.forward(request, response);
                    break;
                default:
                    response.sendRedirect("./404");
                    break;

            }

            //System.out.println("time counter for frontend :"+elapsedSeconds);
        } catch (IOException | ServletException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
